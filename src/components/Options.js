import React from "react";
import Option from "./Option";
/*
class Options extends React.Component {
  
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
      <button onClick={this.props.handleDeleteOptions}>Remove All</button>
        <ol>
          {this.props.options.map((item) => (<Option key={item} optionText={item} />))}
        </ol>
        
      </div>
    );
  }
}
*/

const Options = (props) => (
  <div>
    <div className="widget-header">
      <h3 className="widget-header__title">Your options</h3>
      <button
        className="button button--link"
        onClick={props.handleDeleteOptions}
      >
        Remove All
      </button>
    </div>
    {props.options.length === 0 && (
      <p className="widget__message">Please add an option to get started</p>
    )}
    {props.options.map((item, index) => (
      <Option
        key={item}
        optionText={item}
        count={index + 1}
        handleDeleteOption={props.handleDeleteOption}
      />
    ))}
  </div>
);

export default Options;
