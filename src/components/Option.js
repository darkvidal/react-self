import React from "react";
/*
class Option extends React.Component {
    render() {
        return (
            <li>{this.props.optionText} <button onClick={this.props.handleDeleteOption}>Remove</button></li>
        )
    }
}
*/
const Option = (props) => (
  <div className="option">
    <p className="option__text">{props.count}. {props.optionText}</p>
    <button
      className="button button--link"
      onClick={(e) => {
        props.handleDeleteOption(props.optionText);
      }}
    >
      Remove
    </button>
  </div>
);

export default Option;
