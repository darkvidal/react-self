import React from "react";
import Action from "./Action";
import Header from "./Header";
import Options from "./Options";
import AddOption from "./AddOption";
import OptionModal from "./OptionModal";

class IndesicionApp extends React.Component {
  // STEPS TO HANDLE STATE
  // 1. Setup default state object
  // 2. Component rendered with default state values
  // 3. Change state based on event
  // 4. Component re-rendered using new state values
  // 5. Start again at step 3

  // https://reactjs.org/docs/state-and-lifecycle.html

  state = {
    options: [],
    selectedOption: undefined,
  };

  /*
    constructor(props) {
        super(props)
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this)
        this.handleDeleteOption = this.handleDeleteOption.bind(this)
        this.handlePick = this.handlePick.bind(this)
        this.handleAddOption = this.handleAddOption.bind(this)
        
        //this.state = {
            //title: "Indesicion App",
            //subtitle: "Whatever you want!",
            //This is set for the definition IndesicionApp.defaultProps 
            //options: props.options
            //options: []
        //}        
    }
    */

  //Modifying this to the new "class properties sintax" in order to bind this methods in the constructor
  handleDeleteOptions = () => {
    //handleDeleteOptions() {
    /*
        this.setState(() => {
            return {
                options: []
            }
        })
        */
    this.setState(() => ({ options: [] }));
  };

  handleDeleteOption = (optionToRemove) => {
    //handleDeleteOption(optionToRemove) {
    //console.log(optionToRemove)
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => option !== optionToRemove),
    }));
  };

  handlePick = () => {
    //handlePick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    //console.log(this.state.options[randomNum])
    this.setState(() => ({
      selectedOption: this.state.options[randomNum],
    }));
  };

  handleClearSelectedOption = () => {
    this.setState(() => ({ selectedOption: undefined }));
  };

  handleAddOption = (option) => {
    //handleAddOption(option) {
    //console.log(option)
    if (!option) {
      return "Introduce a valid option to add";
    } else if (this.state.options.indexOf(option) > -1) {
      return "Option already exists";
    }

    this.setState((prevState) => ({
      options: prevState.options.concat(option),
    }));
  };

  componentDidMount = () => {
    //componentDidMount() {
    //Used to fetch data
    //console.log('componentDidMount!')
    try {
      const options = JSON.parse(localStorage.getItem("options"));
      if (options) {
        this.setState(() => ({ options }));
      }
    } catch (ex) {
      //Do nothing at all
    }
  };

  componentDidUpdate(prevProps, prevState) {
    //Used to save data
    //console.log('componentDidUpdate!')
    //console.log(prevProps)
    //console.log(prevState)
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem("options", json);
      //console.log(localStorage.getItem('options'))
    }
  }

  componentWillUnmount() {
    console.log("componentWillunmount!");
  }

  render() {
    const title = "Indesicion App";
    const subtitle = "Whatever you want!";
    //const options = ['Option 1', 'Option 2', 'Option 3']

    return (
      <div>
        <Header title={title} subtitle={subtitle} />
        <div className="container">
          <Action
            hasOptions={this.state.options.length > 0}
            handlePick={this.handlePick}
          />
          <div className="widget">
            <Options
              options={this.state.options}
              handleDeleteOptions={this.handleDeleteOptions}
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption handleAddOption={this.handleAddOption} />
          </div>
        </div>
        <OptionModal
          selectedOption={this.state.selectedOption}
          handleClearSelectedOption={this.handleClearSelectedOption}
        />
      </div>
    );
  }
}

IndesicionApp.defaultProps = {
  options: [],
};

export default IndesicionApp;
