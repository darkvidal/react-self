import React from "react";
// import ReactDOM from 'react-dom'
import { createRoot } from "react-dom/client";
import IndesicionApp from "./components/IndesicionApp";
import './styles/styles.scss'
import 'normalize.css/normalize.css'

/*
    "babel-cli": "^6.26.0",
    "babel-loader": "^7.1.5",
    "babel-preset-env": "^1.7.0",
    "babel-preset-react": "^6.24.1",
*/

/*
class OldSyntax {
    constructor() {
        this.name = 'Servidor De Nadie'
    }
}

class NewSyntax {
    name = 'Master Cerati'
}

const newSyntax = new NewSyntax();

const oldSyntax = new OldSyntax()
console.log(oldSyntax)

console.log(newSyntax)
*/

//ReactDOM.render(jsx, document.getElementById('app')) //OLD WAY!!!!

// https://www.npmjs.com/package/react-modal

const root = createRoot(document.getElementById("app"));
root.render(<IndesicionApp options={["Option 1", "Option 2", "Option 3"]} />);
